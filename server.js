#!/usr/bin/env node
"use strict"; 

import fs from 'fs';
import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from  'cookie-parser';

let app = express();

let config = {
	"port": 7780
}

app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static('./public'));

var server = app.listen(config.port, function () {
  console.log('Frontend server listening on port ' + config.port + '!')
});
