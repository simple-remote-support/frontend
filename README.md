# Frontend

This project contains the static files for the frontend of the web app only.
Can be served with any https file server like nginx or apache2.

## Getting started with development

Install NodeJS and npm on your developer machine.  
  
Install Nodemon

```
npm install -g nodemon
```
Checkout the source code and install its dependencies:
```
cd /path/to/your/working/folder
git pull git@gitlab.com:simple-remote-support/frontend.git
cd frontend
npm install
```
run frontend with the command 
```
nodemon
```

Open this folder in your favorite html IDE.  
site is now available on [http://localhost:7780/index.html](http://localhost:7780/index.html)
