const { app, BrowserWindow } = require('electron');
const { spawn } = require('child_process');

const createWindow = () => { 
	let debug = process.argv.includes("debug");
  const win = new BrowserWindow({
    width: debug ? 1920 : 1280,
    height: 800,
    webPreferences: {
		  additionalArguments: []
		}
  });
  if (debug){
  	win.webContents.openDevTools();
		setTimeout(()=>win.loadFile('index.html', { query: {app: true} }) , 1000);
  } else {
	  win.loadFile('index.html', { query: {app: true} });
  }
}

app.whenReady().then(() => {
  createWindow();
  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

app.on('window-all-closed', () => {  
  if (process.platform !== 'darwin') app.quit();
})

const input = spawn('../../input/bin/srsinput', [],  { stdio: 'inherit', stderr: 'inherit' });
//input.stdout.ondata = (data)=>console.log(data);
//input.stderr.pipe(process.stderr);
input.on('close', code => console.log(`child process exited with code ${code.toString()}`));
process.on('exit', () => input.kill());
