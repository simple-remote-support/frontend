"use strict";

import tools from "./tools.js";
import types from "./types.mjs";
import RTCChannel from "./rtcchannel.js";

// message bytes for interop with srsinput
const MOUSEMOVEMESSAGE = 1
const MOUSEBUTTONMESSAGE = 2
const KEYPRESSEDMESSAGE = 3
const REQUESTCONTROL = 4
const CONTROLGRANTED = 5
const CONTROLDENIED = 6
const CONNECTING = 0
const OPEN = 1
const CLOSING = 2
const CLOSED = 3

/*
* Signal channel objects establish a connection to the 
*/
class SignalChannel {
	constructor(camStream, publicHost, channelId, userId, debug){
		let protocol = location.protocol === "https:" ? "wss://" : "ws://";
		let host = location.host;
		if (debug) {
			host = publicHost + ":7781";  
			protocol = "ws://";
			console.log("debug host " + host);
		} 
		this.userId = userId;
		this.timerId = null;
		this.ws = new WebSocket(protocol + host + "/websocket");
		this.rtcChannels = [];
		this.instanceId = null;
		this.publicHost = publicHost;
		this.desktopStream = null;
		this.camStream = camStream;
		this.inputWs = null;
		this.remoteInput = false;
		this.remoteControlGranted = false;
		this.ws.onopen = async() => {
			let msg = {
			  t: types.MsgTypes.SignalChannelRequest, 
			  userId: userId,    
			  channelId: channelId
			}
			this.ws.send(JSON.stringify(msg));
			this.timerId = setInterval(()=>this.keepAlive, 2000);
		}
		this.ws.onclose = (e) => {
			clearInterval(this.timerId);
			signalChannel = null;
			window.location = "index.html";
		}
		this.ws.onmessage = (e) => this.onMessage(e);
		this.ws.onerror = (e) => this.onError(e);		
	}
	initInputWebsocket(){
		this.inputWs = new WebSocket("ws://localhost:7784");
		this.inputWs.onerror = (e) => {
			this.inputWs = null;
		}
		this.inputWs.onclose = (e) => {	
			this.inputWs = null;			
		}
		this.inputWs.onmessage =  async (e) => {
			let edata = null;
			if (e.data instanceof Blob) { edata = await e.data.text(); }
			else edata = e.data;
			let json = JSON.parse(edata);
			this.onInputWSMessage(json);
		}
	}
	onDesktopShareStopped(){}
	onInputWSMessage(data) {
		switch (data.d) {
			case CONTROLGRANTED:
				console.log("CONTROLGRANTED");
				this.send(JSON.stringify({t:types.MsgTypes.ControlGranted}));
				break;
			case CONTROLDENIED:
				console.log("CONTROLDENIED");
				this.send(JSON.stringify({t:types.MsgTypes.ControlDenied}));
				break;
		}
	}
	shareDesktop() {
		for (let i in this.rtcChannels){      
		  if (this.rtcChannels[i]){
		    this.rtcChannels[i].addDesktopStream();
		  }
		}   
	}
	async onMessage(e) {
		let edata = null;
		if (e.data instanceof Blob) { edata = await e.data.text(); }
		else edata = e.data;
		let data = JSON.parse(edata);
		let rtcChannel = null;
		switch (data.t) {
			case types.MsgTypes.ChannelData:
				for (let i in data.participants){
				  let uid = data.participants[i];
				  if (uid != this.userId){
				    if (this.rtcChannels[data.senderUserId]){
				      this.rtcChannels[data.senderUserId].close();
				    }
				    let rtcChannel = new RTCChannel(this, uid, this.publicHost, true);
				    this.rtcChannels[uid] = rtcChannel;
				    rtcChannel.init();
				  }
				}
				this.instanceId = data.instanceId;
				break;
			case types.MsgTypes.RTCChannelOffer:       
				if (this.rtcChannels[data.senderUserId]){        	
				  rtcChannel = this.rtcChannels[data.senderUserId];
				} else {        	
				  rtcChannel = new RTCChannel(this, data.senderUserId, this.publicHost);
				  rtcChannel.init()
				}

				this.rtcChannels[data.senderUserId] = rtcChannel;
				rtcChannel.remoteUserName = data.userName;
				rtcChannel.onOffer(data);
				break;
			case types.MsgTypes.RTCChannelAnswer:
				rtcChannel = this.rtcChannels[data.senderUserId];        
				if (rtcChannel) {
					rtcChannel.onAnswer(data);
					rtcChannel.remoteUserName = data.userName;
				}
				break;
			case types.MsgTypes.ICECandidate:
				rtcChannel = this.rtcChannels[data.senderUserId];
				if (rtcChannel) rtcChannel.addIceCandidate(data.iceCandidate);
				break;
			case types.MsgTypes.UserLeft:
				rtcChannel = this.rtcChannels[data.userId];
				delete this.rtcChannels[data.userId];
				if (rtcChannel) rtcChannel.close();				
				break;  
			case types.MsgTypes.DesktopShareStopped:
				this.onDesktopShareStopped();
				break;
			case types.MsgTypes.MOMSaved:
				setTimeout(()=>momEditor.load(), 500);
				break;
			case types.MsgTypes.MouseMoveData:
				if (this.inputWs && this.inputWs.readyState == OPEN){
					let bytearray = new Int8Array([MOUSEMOVEMESSAGE,data.relative.x,data.relative.y]);
					this.inputWs.send(bytearray.buffer);
				}      	      	
				break;
			case types.MsgTypes.MouseButtonData:
				if (this.inputWs && this.inputWs.readyState == OPEN	){
					let bytearray = new Int8Array([MOUSEBUTTONMESSAGE,data.button,data.value]);
					this.inputWs.send(bytearray.buffer);
				}      	      	
				break;
			case types.MsgTypes.KeyData:
				if (this.inputWs && this.inputWs.readyState == OPEN	){
					let bytearray = new Uint8Array(data.code.length+2);
					bytearray.set([KEYPRESSEDMESSAGE], 0);
					bytearray.set([data.value], 1);
					for (let i = 0; i < data.code.length; i++) {
						bytearray.set([data.code.charCodeAt(i)], 2+i);
					}
					this.inputWs.send(bytearray.buffer);
				}      	      	
				break;      
			case types.MsgTypes.RequireControl:	
				if (this.inputWs && this.inputWs.readyState == OPEN	){
					let bytearray = new Uint8Array([REQUESTCONTROL]);
					this.inputWs.send(bytearray.buffer);      		
				} else {
					let d = {
						t: types.MsgTypes.ControlDenied, 
						msg: "client has no virtual input device."
					}
					this.send(JSON.stringify(d));
				}
				break;
			case types.MsgTypes.ControlDenied:
				remoteControlGranted = false;
				document.exitPointerLock();
				break;
			case types.MsgTypes.ControlGranted:
				remoteControlGranted = true;
				break;
		}
	}
	onError(evt) {
		console.log(evt);
	}
	send(msg) {
		this.ws.send(msg);
	}
	keepAlive() {
		this.ws.send(JSON.stringify({ t: types.MsgTypes.Ping }));
	}
	onWebRTCConnectionFailed(wrtcc) {
		if (wrtcc.remoteUserId in this.rtcChannels){      
		  wrtcc.close();   
		  if (wrtcc.caller == true){
		    let rtcChannel = new RTCChannel(this, wrtcc.remoteUserId, this.publicHost, true);
		    this.rtcChannels[wrtcc.remoteUserId] = rtcChannel;      
		  }      
		}    
	}
	close() {
		this.ws.close();
	}
}

export default SignalChannel;
