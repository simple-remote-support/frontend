"use strict";

import tools from "./tools.js";
import types from "./types.mjs";
import { createVideoControlButton, createWebCamPlayer, onDesktopShareStarted } from "./mtools.js";
import SignalChannel from "./signalchannel.js";

let userId = null;
let signalChannel = null;
let channelId = null;
let instanceId = null;
let channelName = "Meeting";
let localVideoPlayer = null;
let debug = false;
let micOnly = false;
let filesShown = false;
let chatShown = false;
let userName = "Anonymous";
let publicHost = "srs.pracedru.dk";
let settings = localStorage.getItem("settings");
let onInputWSMessage = (data) => { console.log(data); }
if (settings) userName = JSON.parse(settings).name;

document.addEventListener("pointerlockchange", (e) => { signalChannel.remoteInput = document.pointerLockElement !== null }, false);
let host = window.location.host;

if (host.indexOf("localhost") != -1 || host.indexOf("dev.") != -1 || host === "" ){
  debug = true;
}

window.onContextBackgroundClicked = () => {
	document.getElementById("context_container").innerHTML = "";
}

window.onFullScreen = (sender) => {
	if (tools.isFullscreen){
		document.exitFullscreen();
	} else {
		document.documentElement.requestFullscreen();
	}
}

tools.onFullScreenChanged = () => {
	let desktopShare = document.getElementById("desktop_share");
	let fullscreenButton = document.getElementById("fullscreen");
	let meetingControls = document.getElementById("meeting_controls");
	if (tools.isFullscreen){
		fullscreenButton.value = "Window";
		tools.translateItem(fullscreenButton);
		console.log("fullScreen")	
		if (desktopShare){
			desktopShare.style.maxHeight = "100%";
			desktopShare.style.top = "0";
		}
		if (meetingControls){
			meetingControls.classList.add("faded_controls");
		}
	} else {
		fullscreenButton.value = "Fullscreen";
		console.log("not fullScreen")
		if (desktopShare){
			desktopShare.style = "";
		}
		if (meetingControls){
			meetingControls.classList.remove("faded_controls");
		}
	}
}

let captureConstraint = {
  audio: true,
  video: {
    facingMode: "user",
    frameRate: { ideal: 30 },
    width: { ideal: 320, max: 320 },
    height: { ideal: 240 }
  }
}

window.init = () => {
	tools.applyTheme();
  let urlParams = new URLSearchParams(window.location.search);
  let channelNameItem = document.getElementById("channel_name");
  userId =  urlParams.get('userId');
  channelId = urlParams.get('channelId');
  channelName = urlParams.get('channelName');
  micOnly = urlParams.get('micOnly')==="true";
  document.title = "Meeting " + channelName;
  channelNameItem.innerHTML = channelName;
  startVideoStream();  
	tools.translate(document.body);
}

window.onInvite = async (e) => {
	let protocol = location.protocol === "https:" ? "https://" : "http://";
  let host = location.host;
	let link = protocol + location.host + "/index.html";
	if (location.protocol == "file:") link = `https://${publicHost}/index.html`;
	link += "?channelId=" + channelId;
	link += "&invite=true";
	link += "&userName=" + encodeURIComponent(userName);
	navigator.clipboard.writeText(link);
	let pos = { x: e.x - 150, y: e.y - 120 }
	await tools.loadContext("inviteContext", pos);
  let contextMenu = document.getElementById("context_menu");
	window.setTimeout(()=>{ window.onContextBackgroundClicked(); }, 6000);
}

window.onFilesTabClicked = () => {
  let filesContainerItem = document.getElementById("files_container");
  if (filesShown){
    filesContainerItem.style.animation = "momOut ease 0.5s";
    filesContainerItem.style.right = "calc(0em - var(--mom-width))";
  } else {
    filesContainerItem.style.animation = "momIn ease 0.5s";
    filesContainerItem.style.right = "0em";
  }
  filesShown = !filesShown;
}

window.onChatTabClicked = () => {
  let chatContainerItem = document.getElementById("chat_container");
  if (chatShown){
    chatContainerItem.style.animation = "momOut ease 0.5s";
    chatContainerItem.style.right = "calc(0em - var(--mom-width))";
  } else {
    chatContainerItem.style.animation = "momIn ease 0.5s";
    chatContainerItem.style.right = "0em";
  }
  chatShown = !chatShown;
}

function onDesktopShareStopped(){
  let videoContainerItem = document.getElementById("video_container")
  videoContainerItem.style.display = "";
  videoContainerItem.classList.add("fade_in");
  let contentItem = document.getElementById("desktop_container"); 
  contentItem.innerHTML = "";
  document.getElementById("share_desktop_button").value = tools.translateString("Share desktop");
  if (signalChannel){
  	signalChannel.remoteInput = false;
		for (let i in signalChannel.rtcChannels){      
		  if (signalChannel.rtcChannels[i]){
		    signalChannel.rtcChannels[i].remoteDesktopStream = null;
		  }
		} 
		if (signalChannel.inputWs) signalChannel.inputWs.close();
  } 
}

window.startDesktopStream = async (sender) => {
  let contentItem = document.getElementById("desktop_container"); 
  if (!signalChannel.desktopStream) {    
    try {
	    if (localStorage.app){
	    	let displayMediaOptions = {
		      video: {
						mandatory: {
							chromeMediaSource: 'desktop'
						}
					},
		      audio: {
			      mandatory: {
							chromeMediaSource: 'desktop'
						}
		      } 
		    }
		    signalChannel.desktopStream = await navigator.mediaDevices.getUserMedia(displayMediaOptions);
	    } else {
  			let displayMediaOptions = {
					video: {
						cursor: "always"
					},
					audio: false
				}
	    	signalChannel.desktopStream = await navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
	    }            
      sender.value = tools.translateString("Stop sharing");
      let videoPlayer = document.createElement("video");    
      let videoContainer = document.createElement("div");
      videoPlayer.classList.add("desktop_share");
      videoPlayer.classList.add("fade_in");
      videoPlayer.id = "desktop_share";  		
      videoPlayer.setAttribute('playsinline', 'true');
      contentItem.innerHTML = "";
      videoContainer.appendChild(videoPlayer);
      contentItem.appendChild(videoContainer);
      videoPlayer.srcObject = signalChannel.desktopStream;
      videoPlayer.onloadedmetadata = e => videoPlayer.play();
      videoPlayer.muted = true;     
      signalChannel.shareDesktop(signalChannel.desktopStream);
      signalChannel.initInputWebsocket();
      onDesktopShareStarted();      
    } catch (err) {
      console.log(err);
    }
  } else {  
    sender.value = tools.translateString("Share desktop");
    if (signalChannel.desktopStream){
      for (let i in signalChannel.rtcChannels){      
        if (signalChannel.rtcChannels[i]){
          signalChannel.rtcChannels[i].removeDesktopStream();
        }
      }   
    
      let tracks = signalChannel.desktopStream.getTracks();
      for (let i in tracks){
        let track = tracks[i];
        track.stop();
      }
      signalChannel.desktopStream = null;
      let msg = {
        t: types.MsgTypes.DesktopShareStopped, 
        userId: userId,
        ticket: localStorage.ticket,      
        channelId: channelId
      }
      signalChannel.send(JSON.stringify(msg));
    }    
    onDesktopShareStopped();
  }
}
 

async function startVideoStream(){  
  let contentItem = document.getElementById("video_container"); 
  try {    
    if (micOnly) {
      captureConstraint.video = false;
    }
    let camStream = await navigator.mediaDevices.getUserMedia(captureConstraint);
    let micButton = createVideoControlButton("icons/channel/mic.svg", "icons/channel/micoff.svg");
    let camButton = createVideoControlButton("icons/channel/cam.svg", "icons/channel/camoff.svg");
    micButton.onclick = () => {
      let tracks = camStream.getAudioTracks();
      let enabled = true;      
      for (let i in tracks){
        tracks[i].enabled = !tracks[i].enabled;
        enabled = tracks[i].enabled;
      }
      micButton.switch(enabled);
    }
    camButton.onclick = () => {
      let tracks = camStream.getVideoTracks();
      let enabled = true;
      for (let i in tracks){
        tracks[i].enabled = !tracks[i].enabled;
        enabled = tracks[i].enabled;
      }      
      camButton.switch(enabled);
    }
    let controls = [ micButton ];
    if (!micOnly) controls.push(camButton);
		document.getElementById("video_container").innerHTML = ""; 
    let videoPlayer = createWebCamPlayer(controls, "", userName);   
    videoPlayer.srcObject = camStream;
    videoPlayer.muted = true;
    videoPlayer.onloadedmetadata = e => {
    	 videoPlayer.play();
    }
    
    localVideoPlayer = videoPlayer;
    signalChannel = new SignalChannel(camStream, publicHost, channelId, userId, debug);
    signalChannel.onDesktopShareStopped = onDesktopShareStopped;
  } catch (e){
    console.log(e);
    contentItem.innerHTML = "<h1>Technical Error</h1>";
  }
}

window.closeVideoChat = () => {
  let rtcChannels = signalChannel.rtcChannels;
  let camStream = signalChannel.camStream;
  if (signalChannel){
    signalChannel.close();
    signalChannel = null;
  }  
  for (let i in rtcChannels){
    rtcChannels[i].close();
    delete rtcChannels[i];
  }
  if (camStream){
    camStream.getTracks().forEach(function(track) {
      track.stop();
    });    
  }
  window.location = "index.html";    
}

