"use strict";

import tools from "./tools.js";
import types from "./types.mjs";

function createVideoControlButton(srcon, srcoff){
  let buttonItem = document.createElement("div");
  let iconItem = new Image();// document.createElement("img");
  iconItem.src = srcon;
  iconItem.style.height = "2em";
  buttonItem.classList.add("video_control_button")
  buttonItem.appendChild(iconItem);
  buttonItem.setAttribute("srcon", srcon);
  buttonItem.setAttribute("srcoff", srcoff);
  buttonItem.switch = (value) => {
    iconItem.src = value ? srcon : srcoff;
  }
  return buttonItem;
} 

function createWebCamPlayer(ctrls, id, username){
	let videoPlayer = document.createElement("video");
  let contentItem = document.getElementById("video_container"); 
  let videoContainer = document.createElement("div");
  let controls_container = document.createElement("div");
  let controls = document.createElement("div");
  controls_container.appendChild(controls);
  controls_container.style.position = "relative";
  controls.classList.add("video_controls");
  
  for (let i in ctrls){
	  controls.appendChild(ctrls[i]);
  }
  
  videoContainer.appendChild(controls_container);
  videoContainer.classList.add("video_container");
  videoContainer.classList.add("zoom_in");
  
  let avatar = document.createElement("img");
	avatar.src = "icons/avatarDark.svg";
	avatar.style.position = "absolute";
	avatar.style.top = "45px";
	avatar.style.left = "0px";
	avatar.style.width = "300px";
	videoContainer.appendChild(avatar); 
  
  videoPlayer.classList.add("video_chat");
  videoPlayer.classList.add("fade_in"); 
  videoPlayer.classList.add("zoom_in");    
  videoPlayer.setAttribute('playsinline', 'true');        
  videoPlayer.id = id;
  videoContainer.appendChild(videoPlayer);
  
  let name = document.createElement("h2");
	name.innerHTML = username;
	name.style.position = "relative";
	name.style.top = "-465px";
	name.style.color = "white";
	name.style.filter = "drop-shadow(0px 0px 3px #000)";				
	videoContainer.appendChild(name);       
	  
  contentItem.appendChild(videoContainer);
  return videoPlayer;
}

function onDesktopShareStarted(){
  let videoContainerItem = document.getElementById("video_container")
  videoContainerItem.classList.remove("fade_in");
  videoContainerItem.style.display = "none";
  tools.onFullScreenChanged();
}

export { createVideoControlButton, createWebCamPlayer, onDesktopShareStarted }
