"use strict";

const defaultSettings = {
	name: "Anonymous",
	theme: "System"
}

function detectFullScreen() {
  return (window.fullScreen) || (window.innerWidth == screen.width && window.innerHeight == screen.height);
}

class Tools{
	constructor(){
		this.mViewPath = "views/";
		this.mContextPath = "contexts/";
		this.mFormPath = "forms/";
		this.mContextId = "context_container";
		this.mFormId = "form_container";
		this.mViewId = "view_container";
		this.mPopupId = "popup_container"
		this.lightTheme = "Light"; 
		this.darkTheme = "Dark";
		this.isFullscreen = false;
		this.darkMode = false;
		this.tr = localStorage.tr === undefined ? null : JSON.parse(localStorage.tr);
		document.addEventListener('fullscreenchange', () => {
				this.isFullscreen = !!document.fullscreen;
				this.onFullScreenChanged();
		}, false);
		document.addEventListener('mozfullscreenchange', () => {
				this.isFullscreen = !!document.mozFullScreen;
				this.onFullScreenChanged();  
		}, false);
		document.addEventListener('webkitfullscreenchange', () => {
				this.isFullscreen = !!document.webkitIsFullScreen;
				this.onFullScreenChanged();
		}, false);
		window.onresize = (event) => {
			if (detectFullScreen() != this.isFullscreen){
				this.isFullscreen = !this.isFullscreen;
				this.onFullScreenChanged();
			}
		}
		if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
    	this.darkMode = true;
    	window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', event => {    		
		    this.darkMode = event.matches;
		    this.onDarkModeChanged();
		    if (this.settings.theme == "System")
		    	this.theme = "System";
			});
		}
		this.getTranslation(navigator.language.toLowerCase());
		window.onContextBackgroundClicked = this.onCloseContext;
		window.onPopupBackgroundClicked = this.onClosePopup;
	}
	init (){}
	set viewContainerId(value) { this.mViewId = value; }
	set contextContainerId(value) { this.mContextId = value; }
	set formContainerId(value) { this.mFormId = value; }
	set viewPath(value) { this.mViewPath =  value.charAt(value.length-1) == "/" ? value : value + "/"; }
	set contextPath(value) { this.mContextPath =  value.charAt(value.length-1) == "/" ? value : value + "/"; }
	set formPath(value) { this.mFormPath =  value.charAt(value.length-1) == "/" ? value : value + "/"; }	
	get isMobile() {	return false; }
	get settings() {
		let value = localStorage.getItem("settings");
		if (value === null) value = defaultSettings; 
		else value = JSON.parse(value);
		return value;
	}
	set settings(value){
		localStorage.setItem("settings", JSON.stringify(value));
	}
	
	set theme(value){
		let s = this.settings;
		s.theme = value;
		this.settings = s;
		this.applyTheme();
	}
	onDarkModeChanged() { console.log("system theme changed"); }
	onCloseContext(){
		document.getElementById("context_container").innerHTML = "";
	}
	onClosePopup(){
		document.getElementById("popup_container").innerHTML = "";
	}
	translate(element){
		if (this.tr === null) return;
		let translateItems = element.getElementsByClassName("translate");

		for (let i in translateItems){
			let elementItem = translateItems[i];
			this.translateItem(elementItem);
		}
	}
	closeContext() { this.onCloseContext(); }
	closePopup() { this.onClosePopup(); }
	translateItem(elementItem){
		if (this.tr === null) return;
		switch (elementItem.nodeName){
			case "H1":
			case "H2":
			case "H3":				
			case "SPAN":
			case "TD":
			case "TH":				
			case "BUTTON":
			case "DIV":					
				elementItem.innerText = this.translateString(elementItem.innerText);
				break;
			case "INPUT":
				if (elementItem.type !== "text") elementItem.value = this.translateString(elementItem.value);
				elementItem.placeholder = this.translateString(elementItem.placeholder);
				break;
		}
	}
	translateString(string){
		if (this.tr === null) return string;
		return (string in this.tr) ? this.tr[string] : string;
	}
	async getTranslation(translationName){
		let doTranslate = false;
		try {
			this.tr = await this.getJSON("./tr/" + translationName + ".json");
			if (localStorage.tr == null){
			 	doTranslate = true;
			}
			localStorage.setItem("tr", JSON.stringify(this.tr));	
			if (doTranslate) this.translate(document.body);
			console.log("translations found for: " + translationName);
		} catch (e) {			
			if (translationName.includes("-")){
				console.log("no translations found for " + translationName + " trying alternate");
				this.getTranslation(translationName.split("-")[0]);
			} else console.log("no translations found.");
		}		
	}
	async getHTML(path){
		let options = {
			method: "GET",			
		}
		let response = await fetch(path, options); 
		return await response.text();
	}
	async loadHTML(path, containerId){		
		let container = document.getElementById(containerId);
		container.innerHTML = await this.getHTML(path);
		this.translate(container); 
	}

	async loadView(name, viewId=this.mViewId){
		let path = this.mViewPath + name + ".html";
		await this.loadHTML(path, viewId);
	}
	async loadContext(name, pos){
		let path = this.mContextPath + name + ".html";
		await this.loadHTML(path, this.mContextId);
		let contextMenu = document.getElementById("context_menu");
	  contextMenu.style = "left: " + pos.x + "px; top: " + pos.y + "px;";
	}
	async loadForm(name){
		let popupPath = this.mFormPath + "popup" + ".html";
		let formPath = this.mFormPath + name + ".html";
		let popupPromise = this.getHTML(popupPath, this.mPopupId);
		let formPromise = this.getHTML(formPath, this.mPopupId);
		let popupHTML = await popupPromise;
		let formHTML = await formPromise;
		let popupContainer = document.getElementById(this.mPopupId);
		popupContainer.innerHTML = popupHTML;
		let formContainer = document.getElementById(this.mFormId);
		formContainer.innerHTML = formHTML;
		this.translate(popupContainer); 
	}
	async getJSON(path){
		let options = {
			method: "GET",
			headers: {
			}
		}
		let response = await fetch(path, options);
		let text = await response.text();
		let retObj = JSON.parse(text);
		return retObj;		
	}
	async postJSON(path, sendObject){
		let options = {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(sendObject)
		}
		let response = await fetch(path, options);
		let text = await response.text();
		let retObj = JSON.parse(await response.text());
		return retObj;
	}
	onFullScreenChanged(){
		console.log("this.onFullScreenChanged();");
	}
	setStyleSheetDisabled(themeId, disabled){
		let cssItem = document.getElementById(themeId);
		if (cssItem) cssItem.disabled = disabled;
	}
	/**
  * Aplies the theme by enabling and disabling the css that corresponds to the theme.
  *
  * @return {null}
  */
	applyTheme(){	
		let themeItems = document.getElementsByClassName("theme");
		let found = false;
		let themeName = this.settings.theme;	
		if (themeName == "System"){
			if (this.darkMode) themeName = this.darkTheme;
			else themeName = this.lightTheme;
		}
		console.log(themeName);
		for (let i = 0 ; i < themeItems.length; i++){
			let themeItem = themeItems[i];
			let name = themeItem.getAttribute("name");
			if (name == themeName) found = true;
		}
		if (!found) return;
		for (let i = 0 ; i < themeItems.length; i++){
			let themeItem = themeItems[i];
			let name = themeItem.getAttribute("name");
			if (name == themeName) themeItem.disabled = false;
			else themeItem.disabled = true;
		}
	  setTimeout(()=>{	    
	    let colorThemeDiv = document.createElement("div");
	    colorThemeDiv.classList.add("app_theme")
	    document.body.appendChild(colorThemeDiv);      
	    const style = getComputedStyle(colorThemeDiv);
	    let color = style.backgroundColor;
	    colorThemeDiv.remove();
	    document.querySelector('meta[name="theme-color"]').setAttribute('content',  color);
	  }, 200);  
	}
}

const tools = new Tools();

export default tools;
