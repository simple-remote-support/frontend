"use strict";

import tools from "./tools.js";
import types from "./types.mjs";
import { createVideoControlButton, createWebCamPlayer , onDesktopShareStarted } from "./mtools.js";

let userName = tools.settings.name;

class RTCChannel {
	constructor(signalChannel, remoteUserId, publicHost, caller=false) {
		this.remoteCamStream = null; //new MediaStream();
		this.remoteDesktopStream = null;
		this.signalChannel = signalChannel;
		this.remoteUserId = remoteUserId;
		this.remoteUserName = "none";
		this.peerConnection = null; //new RTCPeerConnection(iceConf);
		this.videoPlayer = null;
		this.caller = caller;
		this.remoteoa = null;
		this.desktopStreamSender = null;
		this.publicHost = publicHost;
		this.mediaConstraints = {
		  'offerToReceiveAudio': true,
		  'offerToReceiveVideo': true,
		  'iceRestart': true    
		}		
		window.onkeyup = (e) => this.sendKeyboardData(e, 0);
		window.onkeydown = (e) => this.sendKeyboardData(e, 1);
	}  
  addDesktopStream() {
    this.signalChannel.desktopStream.getTracks().forEach(track => {
      this.desktopStreamSender = this.peerConnection.addTrack(track, this.signalChannel.desktopStream);
    });
  }  
  removeDesktopStream() {
    this.peerConnection.removeTrack(this.desktopStreamSender);
  }
  get iceConf () {
  	let ic = {
			'iceServers': [
				{ 
					'urls': `turn:${this.publicHost}:3478`,
					'credential': 'st.alone',
					'username': 'rambo'
				}
			]
		}
  	return ic;
  } 
  init() {
    this.remoteCamStream = new MediaStream();
    this.remoteDesktopStream = null;
    this.peerConnection = new RTCPeerConnection(this.iceConf);    
    this.peerConnection.onicecandidate = (e) => this.onicecandidate(e);
    this.peerConnection.onnegotiationneeded = (e) => this.onnegotiationneeded(e);
    this.peerConnection.onconnectionstatechange = (e) => this.onconnectionstatechange(e);
    this.peerConnection.oniceconnectionstatechange = (e) => this.oniceconnectionstatechange(e);
    this.peerConnection.ontrack = (e) => this.ontrack(e);
    this.remoteoa = null;
    this.signalChannel.camStream.getTracks().forEach(track => {
      this.peerConnection.addTrack(track, this.signalChannel.camStream);
    });
    if (this.signalChannel.desktopStream){
      this.addDesktopStream();
    }
    //this.onInputWSMessage = this.onInputWSMessage;
  }
  async makeCall() {
    try{
      let sdp = await this.peerConnection.createOffer(this.mediaConstraints);        
      await this.peerConnection.setLocalDescription(sdp);
      let desktopStreamId = null;
      if (this.signalChannel.desktopStream) desktopStreamId = this.signalChannel.desktopStream.id;
      let offerData = {
        't': types.MsgTypes.RTCChannelOffer,
        'senderUserId': this.signalChannel.userId,
        'recieverUserId': this.remoteUserId,
        'camStreamId': this.signalChannel.camStream.id,
        'desktopStreamId': desktopStreamId,
        'sdp': sdp,
        'userName': userName
      }
      this.signalChannel.send(JSON.stringify(offerData));
    } catch (err) {
      console.log(err);
    }    
  }  
  async onAnswer(answer)  {
    this.remoteoa = answer;
    let remoteDesc = new RTCSessionDescription(answer.sdp);
    try {
      await this.peerConnection.setRemoteDescription(remoteDesc);
    } catch (err) {
      console.log(err);
    }    
  }
  async onOffer(offer) {
    this.remoteoa = offer;
    this.peerConnection.setRemoteDescription(new RTCSessionDescription(offer.sdp));
    let sdp = await this.peerConnection.createAnswer();
    await this.peerConnection.setLocalDescription(sdp);
    let desktopStreamId = null;
    if (this.signalChannel.desktopStream) desktopStreamId = this.signalChannel.desktopStream.id;
    let answerData = {
      't': types.MsgTypes.RTCChannelAnswer,
      'senderUserId': this.signalChannel.userId,
      'recieverUserId': this.remoteUserId,
      'camStreamId': this.signalChannel.camStream.id,
      'desktopStreamId': desktopStreamId,
      'sdp': sdp,
      'userName': userName
    }    
    this.signalChannel.send(JSON.stringify(answerData));
  }
  async addIceCandidate(iceCandidate) {
    try {
        await this.peerConnection.addIceCandidate(iceCandidate);
    } catch (e) {
        console.error('Error adding received ice candidate', e);
    }
  }
  close() {
    this.peerConnection.close();
    if (this.videoPlayer){
      this.videoPlayer.parentElement.remove(); 
      this.videoPlayer = null;
    }    
    delete this.signalChannel.rtcChannels[this.remoteUserId];
    if (this.remoteDesktopStream) {
      this.signalChannel.onDesktopShareStopped();
    }
  }
  onicecandidate(e) {
    if (e.candidate) {
      let iceCandidateData = {
        't': types.MsgTypes.ICECandidate,
        'senderUserId': this.signalChannel.userId,
        'recieverUserId': this.remoteUserId,
        'iceCandidate': e.candidate,
        'userName': userName
      }
      this.signalChannel.send(JSON.stringify(iceCandidateData));
    } else {
    }
  }  
  async onnegotiationneeded(e) {
    console.log("onnegotiationneeded");
    this.makeCall();
  }  
  onconnectionstatechange(e) {
    let cs = this.peerConnection.connectionState;
    switch (cs) {
      case 'failed':
        setTimeout(this.reconnectFollowUp, 2000);
      break;
      default:
        console.log(cs);
    }
  }
  reconnectFollowUp() {
    let cs = this.peerConnection.connectionState;
    if (cs === 'failed'){
      this.makeCall();
    }  else {
      console.log (cs);
    }    
  }
  
  oniceconnectionstatechange(e) {
    let ics = this.peerConnection.iceConnectionState;
    switch (ics) {
      case 'connected':
        console.log("Peers connected");
        if (this.videoPlayer == null){
          let speakerButton = createVideoControlButton("icons/channel/speaker.svg", "icons/channel/speakeroff.svg");
          speakerButton.onclick = () => {
            let tracks = this.remoteCamStream.getAudioTracks();
            let enabled = true;      
            for (let i in tracks){
              tracks[i].enabled = !tracks[i].enabled;
              enabled = tracks[i].enabled;
            }
            speakerButton.switch(enabled);
          }          
          let controls = [ speakerButton ];
          this.videoPlayer = createWebCamPlayer(controls, this.remoteUserId, this.remoteUserName);
          this.videoPlayer.onloadedmetadata = e => {
          	this.videoPlayer.play();
          }
        }
        this.videoPlayer.srcObject = this.remoteCamStream;
      break;
      case 'disconnected':  
        console.log("oniceconnectionstatechange disconnected");        
      break;
      case 'error':
        console.log("oniceconnectionstatechange error");
      break;
      case 'failed':
        console.log("oniceconnectionstatechange failed");        
        this.makeCall();
      break;
    }
  }  
  async ontrack(e) {
    if (e.streams[0].id == this.remoteoa.camStreamId) {      
      this.remoteCamStream.addTrack(e.track);    
    } else if (e.streams[0].id == this.remoteoa.desktopStreamId) {
      console.log("desktop track added")
      this.remoteDesktopStream = new MediaStream();
      this.remoteDesktopStream.addTrack(e.track);    
      let contentItem = document.getElementById("desktop_container"); 
      try {            
        let videoPlayer = document.createElement("video");            
        let videoContainer = document.createElement("div");
        videoPlayer.classList.add("desktop_share");
        videoPlayer.classList.add("fade_in");
        videoPlayer.id = "desktop_share";      	
				videoPlayer.onclick = async () => {
					if (!this.signalChannel.remoteControlGranted){
						this.signalChannel.send(JSON.stringify({t:types.MsgTypes.RequireControl}));
					}
					if (!this.signalChannel.remoteInput) await videoPlayer.requestPointerLock();
				}
				videoPlayer.onmousemove = (e) => this.sendMouseMoveData(e.movementX, e.movementY);
				videoPlayer.onmousedown = (e) => this.sendMouseButtonData(e, 1);
				videoPlayer.onmouseup = (e) => this.sendMouseButtonData(e, 0);			
        videoPlayer.setAttribute('playsinline', 'true');
        videoContainer.appendChild(videoPlayer);
        contentItem.innerHTML = "";
        contentItem.appendChild(videoContainer);
        videoPlayer.srcObject = this.remoteDesktopStream;
        videoPlayer.onloadedmetadata = e => videoPlayer.play();             
        onDesktopShareStarted();
        if (this.signalChannel.desktopStream){
          let tracks = this.signalChannel.desktopStream.getTracks();
          for (let i in tracks){
            let track = tracks[i];
            track.stop();
          }
          this.signalChannel.desktopStream = null;
        }       
      } catch (e){
        console.log(e);
        contentItem.innerHTML = "<h1>Technical Error</h1>";      
      }
    }   
  }
  sendMouseMoveData(dx, dy) {
		if (!this.signalChannel.remoteInput) return;
		let mouseMoveData = {
		  't': types.MsgTypes.MouseMoveData,
		  'senderUserId': this.signalChannel.userId,
		  'recieverUserId': this.remoteUserId,
		  'relative': { x: dx, y: dy }
		}
		this.signalChannel.send(JSON.stringify(mouseMoveData));		
	}
	sendMouseButtonData(e, value) {
		if (!this.signalChannel.remoteInput) return;
		e.preventDefault();
		let button = e.button;
		button = button == 1 ? 2 : button;
		button = button == 2 ? 1 : button;
		let mouseButtonData = {
		  t: types.MsgTypes.MouseButtonData,
		  senderUserId: this.signalChannel.userId,
		  recieverUserId: this.remoteUserId,
		  button: button,
		  value: value
		}
		this.signalChannel.send(JSON.stringify(mouseButtonData));	
	}
	sendKeyboardData(e, value) {		
		if (!this.signalChannel.remoteInput) return;
		e.preventDefault();
		let mouseButtonData = {
		  t: types.MsgTypes.KeyData,
		  senderUserId: this.signalChannel.userId,
		  recieverUserId: this.remoteUserId,
		  code: e.code,
		  value: value
		}
		this.signalChannel.send(JSON.stringify(mouseButtonData));	
	}
}

export default RTCChannel;
