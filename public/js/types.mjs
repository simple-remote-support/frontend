"use strict";

const themes = [
  {name: "Default", css: "defaultcss"},
  {name: "Dark", css: "darkcss"},
  {name: "Rouge", css: "rougecss"},
  {name: "Azure", css: "azurecss"}
];

const DataRoots = {
  User: 0,
  Organization: 1,
  Project: 2,
  Todo: 3,
  Meeting: 4,
  OrgModel: 5
}

const OrgUserTypes = {  
  Owner: 1,  
  Member: 2/*,
  Viewer: 3,*/
}

const SiteUserTypes = { 
  Normal: 0, 
  Admin: 0b1,
  Organizer: 0b10,
  Mentor: 0b100
}

const RequestTypes = {
  FriendRequest: 0,
  MentorRequest: 1,
  JoinProjectRequest: 2,
  JoinOrgRequest: 3,
  CreateAccountRequest: 4,
  AccountVerification: 5,
  AccountRecovery: 6
}

const ViewTypes = {
  Home: 0,
  Communication: 1,
  TaskManagement: 2,
  Questions: 3,
  SettingsUser: 4,
  SettingsOrg: 5,
  SettingsJambo: 6,
  Business: 7,
  BusinessProduct: 8,
  BusinessMarket: 9,
  BusinessModel: 10,
  BusinessTeam: 11,
  BusinessImplementation: 12,
  Frontpage: 13,
  About: 14,
  Contact: 15,
  Policy: 16,
  Admin: 17,
  BackOffice: 18,
  BackOfficeNotes: 19,
  BackOfficeFiles: 20,
  VideoChat: 21
}

const CategoryTypes = {
  Expenses: 0,
  Expenditures: 0,
  Costs: 0,
  Negative: 0,
  Income: 1,
  Capital: 1,
  Assets: 1,
  Positive: 1
}

const Directions = {
  Horizontal: 1,
  Vertical: 2,
  ZDirection: 3,
  ForwardBackward: 4,
  RightLeft: 5,
  UpDown: 6,
  X: 7,
  Y: 8,
}

const Genders = {
  "-": 0,
  "Female": 1,
  "Male": 2,
  "Both female and male": 3,
  toString: function (gender) { return Object.keys(Genders)[gender];  }
}

const Revenues = {
  "-": 0,
  "0 - 10,000": 1,
  "10,000 - 40,000": 2,
  "40,000 - 100,000": 3,
  "100,000 - 200,000": 4,
  "200,000 - 500,000": 5,
  "500,000 - 1,000,000": 6,
  "1,000,000 - 10,000,000": 7,
  "+10,000,000": 8,
  toString: function (revenue) { return Object.keys(Revenues)[revenue]; }
}

const Employees = {
  "-": 0,
  "0-1 employees": 1,
  "2-10 employees": 2,
  "11-50 employees": 3,
  "51-200 employees": 4,
  "201-500 employees": 5,
  "501-1000 employees": 6,
  "+1000 employees": 7,
  toString: function (employees) { return Object.keys(Employees)[employees]; }
}

const DialogTypes = {
  YesNo: 0,
  OkCancel: 1,
  SaveCancel: 2,
  Ok: 3,
  AcceptReject: 4
}

const TaskFilters = {
  MyTodo: 0,
  AllTodo: 1,
  Workspace: 2
}

const CompetitorType = {
  "-": 0,
  Indirect: 1,
  Direct: 2
}

const CompetitorMarketShare = {
  "-": 0,
  None: 1,
  Small: 2,
  Medium: 3,
  Large: 4,
  Dominance: 5
}

const MsgTypes = {
  SignalChannelRequest: 0,
  Ping: 1,
  Pong: 2,
  ChannelData: 3,
  RTCChannelOffer: 4,
  RTCChannelAnswer: 5,
  ICECandidate: 6,
  UserLeft: 7,
  SignalChannelDebugRequest: 8,
  StreamDistributorRequest: 9,
  DesktopShareStopped: 10,
  MOMSaved: 11,
  NotesSaved: 12,
  MouseMoveData: 13,
  MouseButtonData: 14,
  KeyData: 15,
  RequireControl: 16,
  ControlGranted: 17,
  ControlDenied: 18
}

const TaskStatus = {
  NotStarted: 1,
  InProgress: 2,
  Ongoing: 3,
  Waiting: 4,
  Blocked: 5,
  Done: 6,
  GetCaption: (value) => {
    switch (value){
      case TaskStatus.InProgress:
        return "In Progress";
      case TaskStatus.Ongoing:
        return "Ongoing";
      case TaskStatus.NotStarted:
        return "Not Started";
      case TaskStatus.Waiting:
        return "Waiting";
      case TaskStatus.Blocked:
        return "Blocked";       
      case TaskStatus.Done:
        return "Done";
    }
  }
}

const DataHolderTypes = {
  FileHolder: 1,
  FolderHolder: 2
}

const FileHandlerViewType = {
  Grid: 1,
  List: 2
}

const FileSortType = {
  Name: 1,
  UploadDate: 2,
  Uploader: 3,
  Size: 4
}

export default {
  DataRoots: DataRoots,
  OrgUserTypes: OrgUserTypes,
  SiteUserTypes: SiteUserTypes,
  RequestTypes: RequestTypes,
  TaskStatus: TaskStatus,
  MsgTypes: MsgTypes,
  DataHolderTypes: DataHolderTypes
}


