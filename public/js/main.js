"use strict";
import tools from "./tools.js"
//window.tools = tools;
let currentHistoryItem = null;

function clearNavButtons() {
	const collection = document.getElementsByClassName("nav_button");
	for (let i = 0; i < collection.length; i++) {
		collection[i].classList.remove("side_button_selected");
	}
}

function clearSideNavButtons() {
	const collection = document.getElementsByClassName("side_nav_button");
	for (let i = 0; i < collection.length; i++) {
		collection[i].classList.remove("side_nav_button_selected");
	}
}

window.init = () => {	
	tools.applyTheme();
	let urlParams = new URLSearchParams(window.location.search);
  let invite = urlParams.get('invite');
	let app = urlParams.get('app');
	
	if (app === "true") localStorage.setItem("app", true);
	if (invite === "true"){
		window.onJoinClicked();
	} else {
		window.onSupportClicked();
	}	
	tools.translate(document.body);
}

window.onHistoryClicked = () => {
	tools.loadView("history");
	clearNavButtons()
	document.getElementById("historyButton").classList.add("side_button_selected")
}

window.onSettingsClicked = async () => {
	clearNavButtons();
	document.getElementById("settingsButton").classList.add("side_button_selected")
	await tools.loadView("settings");
	document.getElementById("name").value = tools.settings.name;
	document.getElementById("theme").innerText = tools.settings.theme;
}

window.systemSettings = async (sender) => {
	clearSideNavButtons();
	sender.classList.add("side_nav_button_selected");
}

window.onSupportClicked = async () => {
	await tools.loadView("support");
	clearNavButtons()
	document.getElementById("supportButton").classList.add("side_button_selected");
	if (localStorage.settings != null) document.getElementById("name").value = tools.settings.name;
}

window.onJoinClicked = async () => {
	await tools.loadView("join");
	if (localStorage.settings != null) document.getElementById("name").value = tools.settings.name;
	let otherUser = document.getElementById("other_user"); 
	let urlParams = new URLSearchParams(window.location.search);
	let otherUserName = urlParams.get('userName');
  if (otherUserName === null) otherUserName = "Anonymous";
	otherUser.innerText = otherUserName;
}

Element.prototype.insertChildAtIndex = function(child, index) {
  if (!index) index = 0;
  if (index >= this.children.length) {
    this.appendChild(child);
  } else {
    this.insertBefore(child, this.children[index]);
  }
}

window.onHistoryClicked = async () => {
	await tools.loadView("history");
	clearNavButtons();
	document.getElementById("historyButton").classList.add("side_button_selected");
	let history = getHistory();
	let table = document.getElementById("history");
	for (let i in history.items){
		let historyItem = history.items[i];
		let d = new Date(historyItem.date);
		let row = document.createElement("tr");
		let nameCell = document.createElement("td");
		let dateCell = document.createElement("td");
		row.appendChild(nameCell);
		row.appendChild(dateCell);
		nameCell.innerText = historyItem.userName;
		dateCell.innerText = d.toLocaleDateString() + " " + d.toLocaleTimeString();
		table.insertChildAtIndex(row, 1);
		row.onclick = (sender) => {
			currentHistoryItem = historyItem;
			document.getElementById("restartButton").classList.remove("rounded_shade_button_disabled");
		}
	}
}

window.startSupport = onStartMeetingClicked;
window.onJoinMeetingClicked = onJoinMeetingClicked;
window.restartSupport = onRestartSupportClicked;

window.onThemeClicked = async (e) => {
	let pos = { x: e.x - 80, y: e.y - 90 }
	await tools.loadContext("themesContextMenu", pos);
	let contextMenu = document.getElementById("context_menu");
  let themeItems = document.getElementsByClassName("theme");
  let themes = ["System"];
	for (let i = 0 ; i < themeItems.length; i++){
		let themeItem = themeItems[i];
		let name = themeItem.getAttribute("name");
		themes.push(name);
	}
	for (let i in themes){
		let name = themes[i];
	  let themeContextItem = document.createElement("div");
	  themeContextItem.classList.add("context_item");
	  themeContextItem.innerText = name;
	  themeContextItem.onclick = () => { 
			tools.theme = name;
	  }
	  contextMenu.appendChild	(themeContextItem);	
	}
}

window.onNameChanged = (sender) => {
	let s = tools.settings;
	s.name = sender.value;
	tools.settings = s;
}

window.onLanguageClicked = (sender)=>{
}

function getHistory(){
	let history = localStorage.getItem("history");
	if (history === null) history = {items: []};
	else history = JSON.parse(history);
	return history;
}

function openMeetingWindow(micOnly=false, userId, channelId, userName) {
  let url = "videochat.html";
	if (userName !== null){
		let history = getHistory();	
		history.items.push({channelId: channelId, userName: userName, date: Date.now()});
		localStorage.setItem("history", JSON.stringify(history));
	}
  if (tools.isMobile) {
    url = "videochatMobile.html";
  }
  url += "?channelId=" + channelId;
  url += "&userId=" + userId;
  url += '&channelName=' + "Support";
  if (micOnly) url += "&micOnly=true";
  else url += "&micOnly=false";

	window.location = url;
}

async function detectWebcam() {
  let md = navigator.mediaDevices;
  if (!md || !md.enumerateDevices) return false;
  let devices = await md.enumerateDevices();
  return devices.some(device => 'videoinput' === device.kind);
}

async function queryMeetingMode(e, micOnlyCallback, macAndCamCallback){
	let hasWebCam = await detectWebcam();
	let pos = { x: e.x - 80, y: e.y - 90 }
	await tools.loadContext("meetingContextMenu", pos);
  let micButtonItem = document.getElementById("mic_only_menu_button");
  let micCamButtonItem = document.getElementById("mic_cam_menu_button");
  micButtonItem.onclick = micOnlyCallback;
  if (hasWebCam) micCamButtonItem.onclick = macAndCamCallback;
  else micCamButtonItem.innerText = "No camera found";
}

function onStartMeetingClicked(e){
	let userId = crypto.randomUUID();
 	queryMeetingMode(e, 
 		(e) => { tools.closeContext(); openMeetingWindow(true, userId, userId, "You"); },
 		(e) => { tools.closeContext(); openMeetingWindow(false, userId, userId, "You"); }
 	);
}

function onJoinMeetingClicked(e){
  let urlParams = new URLSearchParams(window.location.search);
  let channelId = urlParams.get('channelId');
  let userId = crypto.randomUUID();
  let otherUserName = urlParams.get('userName');
  if (otherUserName === null) otherUserName = "Anonymous";
 	queryMeetingMode(e, 
 		(e) => { tools.closeContext(); openMeetingWindow(true, userId, channelId, otherUserName); },
 		(e) => { tools.closeContext(); openMeetingWindow(false, userId, channelId, otherUserName); }
 	);
}

function onRestartSupportClicked(e){
	let urlParams = new URLSearchParams(window.location.search);
  let channelId = currentHistoryItem.channelId;
  let userId = crypto.randomUUID();
  let otherUserName = currentHistoryItem.userName;
 	queryMeetingMode(e, 
 		(e) => { tools.closeContext(); openMeetingWindow(true, userId, channelId, null); },
 		(e) => { tools.closeContext(); openMeetingWindow(false, userId, channelId, null); }
 	);
}

